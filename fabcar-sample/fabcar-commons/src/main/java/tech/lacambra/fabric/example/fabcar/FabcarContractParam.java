package tech.lacambra.fabric.example.fabcar;

public class FabcarContractParam {
  public static final String CONTRACT_ID = "FABCAR";
  public static final String INIT_LEDGER = "INIT_LEDGER";
  public static final String CREATE_CAR = "CREATE_CAR";
  public static final String QUERY_ALL_CARS = "QUERY_ALL_CARS";
  public static final String CHANGE_CAR_OWNER = "CHANGE_CAR_OWNER";
}
